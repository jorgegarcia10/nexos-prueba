﻿CREATE TABLE Cliente(
	idCliente serial not null primary key,
	nombre varchar(12),
	apellido1 varchar(20),
	apellido2 varchar(20),
	observaciones varchar(500)
);

CREATE TABLE Mesa(
	idMesa serial not null primary key,
	numMaxComensales bigint,
	ubicación varchar(10)
);

CREATE TABLE Camarero(
	idCamarero serial not null primary key,
	nombre varchar(20),
	apellido1 varchar(20),
	apellido2 varchar(20)
	
);

CREATE TABLE Cocinero(
	idCocinero serial not null primary key,
	nombre varchar(20),
	apellido1 varchar(20),
	apellido2 varchar(20)
);

CREATE TABLE Factura(
	idFactura serial not null primary key,
	fechaFactura timestamp,
	idCliente bigint,
	idCamarero bigint,
	idMesa bigint,
	FOREIGN KEY (idCliente) REFERENCES Cliente (idCliente),
	FOREIGN KEY (idCamarero) REFERENCES Camarero (idCamarero),
	FOREIGN KEY (idMesa) REFERENCES Mesa (idMesa)
);

CREATE TABLE DetalleFactura(
	idFactura serial not null,
	idDetalleFactura serial not null,
	idCocinero bigint,
	plato varchar(30),
	importe bigint,
	primary key(idFactura, idDetalleFactura),
	FOREIGN KEY (idCocinero) REFERENCES Cocinero (idCocinero)
);

alter table DetalleFactura add constraint FK_2 FOREIGN KEY (idFactura) REFERENCES Factura(idFactura);

insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Anabal', 'Skeggs', 'Enderwick', 'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Felicity', 'Brunger', 'Oakley', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Eberhard', 'Gilby', 'Glaisner', 'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Doralia', 'Olliver', 'Petry', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Lauri', 'Copelli', 'Shallcroff', 'Etiam pretium iaculis justo. In hac habitasse platea dictumst.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Sammy', 'McGraith', 'Sotheron', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Loni', 'Zaczek', 'Surfleet', 'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Meir', 'Petche', 'Rysom', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Clayson', 'Tippings', 'Stebbings', 'Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.');
insert into Cliente (nombre, apellido1, apellido2, observaciones) values ('Jordan', 'Simonsson', 'Pimley', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.');

insert into Mesa (numMaxComensales, ubicación) values (1, 'Texas');
insert into Mesa (numMaxComensales, ubicación) values (2, 'North C');
insert into Mesa (numMaxComensales, ubicación) values (3, 'Georgia');
insert into Mesa (numMaxComensales, ubicación) values (4, 'Georgia');
insert into Mesa (numMaxComensales, ubicación) values (5, 'Oregon');
insert into Mesa (numMaxComensales, ubicación) values (6, 'Mississipi');
insert into Mesa (numMaxComensales, ubicación) values (7, 'Virginia');
insert into Mesa (numMaxComensales, ubicación) values (8, 'California');
insert into Mesa (numMaxComensales, ubicación) values (9, 'Nebraska');
insert into Mesa (numMaxComensales, ubicación) values (10, 'Carolina');

insert into Camarero (nombre, apellido1, apellido2) values ('Thorvald', 'MacCracken', 'Chapiro');
insert into Camarero (nombre, apellido1, apellido2) values ('Bren', 'Niemetz', 'Treslove');
insert into Camarero (nombre, apellido1, apellido2) values ('Esma', 'Giabucci', 'Elles');
insert into Camarero (nombre, apellido1, apellido2) values ('Maurise', 'Gibbons', 'Sheardown');
insert into Camarero (nombre, apellido1, apellido2) values ('Ogdan', 'Walthew', 'Boldry');
insert into Camarero (nombre, apellido1, apellido2) values ('Claybourne', 'Roseblade', 'Breeder');
insert into Camarero (nombre, apellido1, apellido2) values ('Mahala', 'Hibbart', 'Bruneton');
insert into Camarero (nombre, apellido1, apellido2) values ('Corbet', 'Brayson', 'Timothy');
insert into Camarero (nombre, apellido1, apellido2) values ('Alecia', 'Cherry Holme', 'Tighe');
insert into Camarero (nombre, apellido1, apellido2) values ('Claudell', 'Andreuzzi', 'Leatherborrow');

insert into Cocinero (nombre, apellido1, apellido2) values ('Jenna', 'Judson', 'Dumberrill');
insert into Cocinero (nombre, apellido1, apellido2) values ('Kipper', 'Cardero', 'Hewell');
insert into Cocinero (nombre, apellido1, apellido2) values ('Cortie', 'Croizier', 'Vowden');
insert into Cocinero (nombre, apellido1, apellido2) values ('Brennan', 'Anfusso', 'Mathwin');
insert into Cocinero (nombre, apellido1, apellido2) values ('Lainey', 'Oliver', 'Sturgis');
insert into Cocinero (nombre, apellido1, apellido2) values ('Butch', 'Habden', 'MacQuaker');
insert into Cocinero (nombre, apellido1, apellido2) values ('Jannelle', 'Tiron', 'Halliberton');
insert into Cocinero (nombre, apellido1, apellido2) values ('Purcell', 'Crisford', 'Tolchard');
insert into Cocinero (nombre, apellido1, apellido2) values ('Kevin', 'Mottershaw', 'Veschambes');
insert into Cocinero (nombre, apellido1, apellido2) values ('Alta', 'Dahl', 'Ferrarotti');

insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (1, 1, 7, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (2, 2, 8, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (3, 3, 9, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (4, 4, 10, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (5, 5, 11, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (6, 6, 12, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (7, 7, 13, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (8, 8, 14, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (9, 9, 15, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (10, 10, 16, current_date);

insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (1, 1, 7, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (2, 2, 8, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (3, 3, 9, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (4, 4, 10, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (5, 5, 11, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (6, 6, 12, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (7, 7, 13, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (8, 8, 14, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (9, 9, 15, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (10, 10, 16, current_date);

insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (1, 1, 7, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (2, 2, 8, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (3, 3, 9, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (4, 4, 10, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (5, 5, 11, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (6, 6, 12, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (7, 7, 13, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (8, 8, 14, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (9, 9, 15, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (10, 10, 16, current_date);

insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (1, 1, 7, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (2, 2, 8, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (3, 3, 9, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (4, 4, 10, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (5, 5, 11, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (6, 6, 12, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (7, 7, 13, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (8, 8, 14, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (9, 9, 15, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (10, 10, 16, current_date);

insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (1, 1, 7, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (2, 2, 8, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (3, 3, 9, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (4, 4, 10, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (5, 5, 11, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (6, 6, 12, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (7, 7, 13, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (8, 8, 14, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (9, 9, 15, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (10, 10, 16, current_date);

insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (1, 1, 7, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (2, 2, 8, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (3, 3, 9, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (4, 4, 10, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (5, 5, 11, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (6, 6, 12, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (7, 7, 13, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (8, 8, 14, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (9, 9, 15, current_date);
insert into Factura (idCliente, idCamarero, idMesa, fechaFactura) values (10, 10, 16, current_date);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (2, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (4, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (5, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (6, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (7, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (8, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (9, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (10, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (11, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (12, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (2, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (4, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (5, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (6, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (7, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (8, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (9, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (10, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (11, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (12, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (2, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (4, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (5, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (6, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (7, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (8, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (9, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (10, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (11, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (12, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (13, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (14, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (15, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (16, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (17, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (18, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (19, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (20, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (21, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (22, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (13, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (14, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (15, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (16, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (17, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (18, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (19, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (20, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (21, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (22, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (13, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (14, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (15, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (16, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (17, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (18, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (19, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (20, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (21, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (22, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (23, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (24, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (25, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (26, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (27, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (28, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (29, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (30, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (31, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (32, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (23, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (24, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (25, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (26, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (27, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (28, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (29, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (30, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (31, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (32, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (23, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (24, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (25, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (26, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (27, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (28, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (29, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (30, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (31, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (32, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (33, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (34, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (35, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (36, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (37, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (38, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (39, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (40, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (41, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (42, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (33, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (34, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (35, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (36, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (37, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (38, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (39, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (40, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (41, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (42, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (33, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (34, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (35, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (36, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (37, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (38, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (39, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (40, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (41, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (42, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (43, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (44, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (45, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (46, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (47, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (48, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (49, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (50, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (51, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (52, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (43, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (44, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (45, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (46, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (47, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (48, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (49, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (50, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (51, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (52, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (43, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (44, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (45, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (46, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (47, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (48, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (49, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (50, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (51, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (52, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (53, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (54, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (55, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (56, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (57, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (58, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (59, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (60, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (61, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (62, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (53, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (54, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (55, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (56, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (57, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (58, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (59, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (60, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (61, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (62, 10, 'Queso', 3000);

insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (53, 1, 'Frijoles', 13000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (54, 2, 'Filete', 14000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (55, 3, 'Chuleta', 16000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (56, 4, 'Sopa', 500);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (57, 5, 'Pescado', 19000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (58, 6, 'Carne', 17000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (59, 7, 'Arroz', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (60, 8, 'Papas', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (61, 9, 'Maduros', 3000);
insert into DetalleFactura (idFactura, idCocinero, plato, importe) values (62, 10, 'Queso', 3000);

create view GastosClientesV as select total.* from (select c.idCliente, c.nombre, sum(df.importe) as total from Cliente c, Factura f, DetalleFactura df where c.idCliente = f.idCliente and f.idFactura = df.idFactura group by (c.idCliente)) as total where total > 10000;

create view VentasCamarerosV as select EXTRACT(MONTH FROM f.fechaFactura) as mes, ca.nombre, ca.apellido1, df.idCocinero, sum(df.importe) from Cocinero c, Camarero ca, DetalleFactura df, Factura f where df.idFactura = f.idFactura and f.idCamarero = ca.idCamarero group by f.fechaFactura,df.idCocinero, ca.nombre, ca.apellido1;

