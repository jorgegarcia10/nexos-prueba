Pasos para instalación y ejecución de la aplicación "LA MEJOR COCINA".

1. Ejecutar script de base de datos adjunto (nexosbd.sql) POSTGRESQL.
2. Importar el proyecto dentro del IDE preferido, en mi caso utilicé ECLIPSE, Version: 2019-06 (4.12.0)
3. En caso de querer realizar el despliegue de la aplicación en el servidor web para ambientes productivos, el archivo war lo encuentra en el adjunto de esta carpeta, adicionalmente se deben cambiar las propiedades de conexión a la base de datos en el archivo application.properties ubicado en src/main/resources.
4. Si desea ejecutar la aplicación de forma local, seleccione el proyecto, Run as, Java Application.
5. El servidor levantará la aplicación bajo el contexto /prueba.
6. Debe acceder especificando la ruta de su servidor local y el puerto, continuando con el contexto. (Adicional siguiente paso)
7. La aplicación inicia con el formulario de registro de facturas, este es el endpoint. /prueba/index.
8. Seguido lo llevará a registrar el detalle de la factura.
9. Por medio del endpoint /prueba/verClientes, podrá visualizar los clientes que han gastado más de 10000 pesos en el restaurante.
10. Por medio del endpoint /prueba/verCamareros, podrá visualizar los valores de venta de los camareros por mes.
