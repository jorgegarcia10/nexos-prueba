package com.nexos.prueba.practica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.prueba.practica.model.Camarero;
import com.nexos.prueba.practica.model.Ventascamarerosv;
import com.nexos.prueba.practica.persistence.CamareroRepository;
import com.nexos.prueba.practica.persistence.VentasCamarerosVRepository;

@Service
public class CamareroService {
	
	@Autowired
	private CamareroRepository camareroRepository;
	
	@Autowired
	private VentasCamarerosVRepository ventasCamareroRepository;
	
	public Camarero findByName(String nombre) {
		return camareroRepository.findByNombre(nombre);
	}
	
	public List<Ventascamarerosv> ventas() {
		return ventasCamareroRepository.findAll();
	}
	
	

}
