package com.nexos.prueba.practica.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.prueba.practica.model.Detallefactura;
import com.nexos.prueba.practica.persistence.DetallefacturaRepository;

@Service
public class DetalleFacturaService {
		
	@Autowired
	private DetallefacturaRepository detalleRepo;
	
	public Detallefactura save(Detallefactura detalle) {
		return detalleRepo.save(detalle); 
	}
}
