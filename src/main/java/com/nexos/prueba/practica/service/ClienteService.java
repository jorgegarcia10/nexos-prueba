package com.nexos.prueba.practica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.prueba.practica.model.Cliente;
import com.nexos.prueba.practica.model.Gastosclientesv;
import com.nexos.prueba.practica.persistence.ClienteRepository;
import com.nexos.prueba.practica.persistence.GastosClienteV;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private GastosClienteV gastosClienteRepository;
	
	public Cliente findByName(String nombre) {
		return clienteRepository.findByNombre(nombre);
	}
	
	public List<Gastosclientesv> gastos() {
		return gastosClienteRepository.clientes();
	}
}
