package com.nexos.prueba.practica.service;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.prueba.practica.model.Factura;
import com.nexos.prueba.practica.persistence.FacturaRepository;

@Service
public class FacturaService {
	
	@Autowired
	private FacturaRepository facturaRepository;
	
	public Factura save(Factura factura) {
		return facturaRepository.save(factura);
	}
	
	public Optional<Factura> findById(BigInteger id){
		return facturaRepository.findById(id);
	}
	
}
