package com.nexos.prueba.practica.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.prueba.practica.model.Cocinero;
import com.nexos.prueba.practica.persistence.CocineroRepository;

@Service
public class CocineroService {
	
	@Autowired
	private CocineroRepository cocineroRepo;
	
	public Cocinero findByName(String nombre) {
		return cocineroRepo.findByNombre(nombre);
	}
}
