package com.nexos.prueba.practica.controller;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.nexos.prueba.practica.model.Detallefactura;
import com.nexos.prueba.practica.model.DetallefacturaPK;
import com.nexos.prueba.practica.model.Factura;
import com.nexos.prueba.practica.service.CamareroService;
import com.nexos.prueba.practica.service.ClienteService;
import com.nexos.prueba.practica.service.CocineroService;
import com.nexos.prueba.practica.service.DetalleFacturaService;
import com.nexos.prueba.practica.service.FacturaService;

@Controller
public class FacturaController {
	
	
	@Autowired 
	private FacturaService facturaService;
	
	@Autowired
	private CamareroService camareroService;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private DetalleFacturaService detalleService;
	
	@Autowired
	private CocineroService cocineroService;
	
	@GetMapping("/index")
	public String crearAccion(Factura factura) {
		return "regFact";
	}
	
	@PostMapping("/regFact")
	public String regFactura(Factura factura, Detallefactura detallefactura) {
		
		Factura fact = new Factura();
		fact.setCamarero(camareroService.findByName(factura.getCamarero().getNombre()));
		fact.setCliente(clienteService.findByName(factura.getCliente().getNombre()));
		fact.setFechafactura(new Timestamp(new Date().getTime()));
		
		facturaService.save(fact);
		return "regDet";		
	}	
	
	@PostMapping("/regDet")
	public String regDetalle(Detallefactura detalleFact, Model model) {
		
		Detallefactura detalle = new Detallefactura();
		detalle.setCocinero(cocineroService.findByName(detalleFact.getCocinero().getNombre()));
		detalle.setImporte(detalleFact.getImporte());
		detalle.setPlato(detalleFact.getPlato());
		detalle.setFactura(facturaService.findById(new BigInteger("69")).get());
		
		DetallefacturaPK pk = new DetallefacturaPK();
		pk.setIdfactura((facturaService.findById(new BigInteger("69")).get().getIdfactura()).intValue());
		pk.setIddetallefactura(1);
		
		detalle.setId(pk);
		
		detalleService.save(detalle);
		
		return "success";		
	}
	
	@GetMapping("/verClientes")
	public String verClientes(Model model) {
		model.addAttribute("clientes", clienteService.gastos());
		return "gastosClientes";
	}
	
	@GetMapping("/verCamareros")
	public String verCamareros(Model model) {
		model.addAttribute("camareros", camareroService.ventas());
		return "ventasCamareros";
	}
	
}
