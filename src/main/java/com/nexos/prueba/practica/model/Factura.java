package com.nexos.prueba.practica.model;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import java.math.BigInteger;


/**
 * The persistent class for the factura database table.
 * 
 */
@Entity
@Table(name="factura")
@NamedQuery(name="Factura.findAll", query="SELECT f FROM Factura f")
public class Factura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private BigInteger idfactura;

	private Timestamp fechafactura;

	//bi-directional many-to-one association to Camarero
	@ManyToOne
	@JoinColumn(name="idcamarero")
	private Camarero camarero;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Mesa
	@ManyToOne
	@JoinColumn(name="idmesa")
	private Mesa mesa;
	
	//bi-directional many-to-one association to Detallefactura
	@OneToMany(mappedBy="factura")
	private List<Detallefactura> detallefacturas;
	
	public Factura() {
	}

	public BigInteger getIdfactura() {
		return this.idfactura;
	}

	public void setIdfactura(BigInteger idfactura) {
		this.idfactura = idfactura;
	}

	public Timestamp getFechafactura() {
		return this.fechafactura;
	}

	public void setFechafactura(Timestamp fechafactura) {
		this.fechafactura = fechafactura;
	}

	public Camarero getCamarero() {
		return this.camarero;
	}

	public void setCamarero(Camarero camarero) {
		this.camarero = camarero;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Mesa getMesa() {
		return this.mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public List<Detallefactura> getDetallefacturas() {
		return detallefacturas;
	}

	public void setDetallefacturas(List<Detallefactura> detallefacturas) {
		this.detallefacturas = detallefacturas;
	}

}