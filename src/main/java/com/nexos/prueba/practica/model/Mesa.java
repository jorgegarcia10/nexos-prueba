package com.nexos.prueba.practica.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the mesa database table.
 * 
 */
@Entity
@Table(name="mesa")
@NamedQuery(name="Mesa.findAll", query="SELECT m FROM Mesa m")
public class Mesa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private BigInteger idmesa;

	private Long nummaxcomensales;

	@Column(length=10)
	private String ubicación;

	//bi-directional many-to-one association to Factura
	@OneToMany(mappedBy="mesa")
	private List<Factura> facturas;

	public Mesa() {
	}

	public BigInteger getIdmesa() {
		return this.idmesa;
	}

	public void setIdmesa(BigInteger idmesa) {
		this.idmesa = idmesa;
	}

	public Long getNummaxcomensales() {
		return this.nummaxcomensales;
	}

	public void setNummaxcomensales(Long nummaxcomensales) {
		this.nummaxcomensales = nummaxcomensales;
	}

	public String getUbicación() {
		return this.ubicación;
	}

	public void setUbicación(String ubicación) {
		this.ubicación = ubicación;
	}

	public List<Factura> getFacturas() {
		return this.facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}

	public Factura addFactura(Factura factura) {
		getFacturas().add(factura);
		factura.setMesa(this);

		return factura;
	}

	public Factura removeFactura(Factura factura) {
		getFacturas().remove(factura);
		factura.setMesa(null);

		return factura;
	}

}