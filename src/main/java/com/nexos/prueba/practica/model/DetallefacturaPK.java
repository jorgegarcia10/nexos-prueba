package com.nexos.prueba.practica.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the detallefactura database table.
 * 
 */
@Embeddable
public class DetallefacturaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(unique=true, nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idfactura;

	@Column(unique=true, nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer iddetallefactura;

	public DetallefacturaPK() {
	}
	public Integer getIdfactura() {
		return this.idfactura;
	}
	public void setIdfactura(Integer idfactura) {
		this.idfactura = idfactura;
	}
	public Integer getIddetallefactura() {
		return this.iddetallefactura;
	}
	public void setIddetallefactura(Integer iddetallefactura) {
		this.iddetallefactura = iddetallefactura;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DetallefacturaPK)) {
			return false;
		}
		DetallefacturaPK castOther = (DetallefacturaPK)other;
		return 
			this.idfactura.equals(castOther.idfactura)
			&& this.iddetallefactura.equals(castOther.iddetallefactura);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idfactura.hashCode();
		hash = hash * prime + this.iddetallefactura.hashCode();
		
		return hash;
	}
}