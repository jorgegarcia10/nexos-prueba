package com.nexos.prueba.practica.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the gastosclientesv database table.
 * 
 */
@Entity
@Table(name="gastosclientesv")
@NamedQuery(name="Gastosclientesv.findAll", query="SELECT g FROM Gastosclientesv g")
public class Gastosclientesv implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer idcliente;

	@Column(length=12)
	private String nombre;

	@Column(precision=131089)
	private BigDecimal total;

	public Gastosclientesv() {
	}

	public Integer getIdcliente() {
		return this.idcliente;
	}

	public void setIdcliente(Integer idcliente) {
		this.idcliente = idcliente;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}