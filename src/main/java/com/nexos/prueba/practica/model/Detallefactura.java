package com.nexos.prueba.practica.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detallefactura database table.
 * 
 */
@Entity
@Table(name="detallefactura")
@NamedQuery(name="Detallefactura.findAll", query="SELECT d FROM Detallefactura d")
public class Detallefactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DetallefacturaPK id;

	@ManyToOne
	@JoinColumn(name = "idcocinero")
	private Cocinero cocinero;

	private Long importe;

	@Column(length=30)
	private String plato;

	//bi-directional many-to-one association to Factura
	@ManyToOne
	@JoinColumn(name="idfactura", nullable=false, insertable=false, updatable=false)
	private Factura factura;

	public Detallefactura() {
	}

	public DetallefacturaPK getId() {
		return this.id;
	}

	public void setId(DetallefacturaPK id) {
		this.id = id;
	}

	public Long getImporte() {
		return this.importe;
	}

	public void setImporte(Long importe) {
		this.importe = importe;
	}

	public String getPlato() {
		return this.plato;
	}

	public void setPlato(String plato) {
		this.plato = plato;
	}

	public Factura getFactura() {
		return this.factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Cocinero getCocinero() {
		return cocinero;
	}

	public void setCocinero(Cocinero cocinero) {
		this.cocinero = cocinero;
	}

}