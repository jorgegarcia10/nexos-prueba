package com.nexos.prueba.practica.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ventascamarerosv database table.
 * 
 */
@Entity
@Table(name="ventascamarerosv")
@NamedQuery(name="Ventascamarerosv.findAll", query="SELECT v FROM Ventascamarerosv v")
public class Ventascamarerosv implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(length=20)
	private String apellido1;
	
	@Id
	private Long idcocinero;

	private int mes;

	@Column(length=20)
	private String nombre;

	@Column(precision=131089)
	private BigDecimal sum;

	public Ventascamarerosv() {
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public Long getIdcocinero() {
		return this.idcocinero;
	}

	public void setIdcocinero(Long idcocinero) {
		this.idcocinero = idcocinero;
	}

	public int getMes() {
		return this.mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getSum() {
		return this.sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

}