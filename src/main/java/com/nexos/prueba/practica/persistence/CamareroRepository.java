package com.nexos.prueba.practica.persistence;
import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nexos.prueba.practica.model.Camarero;

@Repository
public interface CamareroRepository extends JpaRepository<Camarero, BigInteger> {
	public Camarero findByNombre(String nombre);
}
