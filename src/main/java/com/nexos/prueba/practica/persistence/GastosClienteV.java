package com.nexos.prueba.practica.persistence;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nexos.prueba.practica.model.Gastosclientesv;

@Repository
public interface GastosClienteV extends JpaRepository<Gastosclientesv, BigInteger>{
	@Query(value = "select gc from Gastosclientesv gc", nativeQuery = false)
		public List<Gastosclientesv> clientes();
}
