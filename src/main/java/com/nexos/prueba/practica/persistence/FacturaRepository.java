package com.nexos.prueba.practica.persistence;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexos.prueba.practica.model.Factura;
@Repository
public interface FacturaRepository extends JpaRepository<Factura, BigInteger> {
	
}
