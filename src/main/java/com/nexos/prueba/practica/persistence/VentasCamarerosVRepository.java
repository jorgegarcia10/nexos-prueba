package com.nexos.prueba.practica.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nexos.prueba.practica.model.Ventascamarerosv;

@Repository
public interface VentasCamarerosVRepository extends JpaRepository<Ventascamarerosv, Long> {
	
	
}
